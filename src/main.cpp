#include "ros/ros.h"
#include "std_msgs/String.h"



//sensors
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Imu.h>

//pcl related
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>

ros::Publisher CloudRepublish;
ros::Publisher ImuRepublish;

void cloudCallback(const sensor_msgs::PointCloud2ConstPtr& cloud_msg){

    sensor_msgs::PointCloud2 relayed;
    pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2;
    pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
    pcl::PCLPointCloud2 buffer;

    //convert to pcl
    pcl_conversions::toPCL(*cloud_msg,*cloud);

    //perform any required middleware ops if any, filtering etc
    pcl_conversions::fromPCL(*cloud, relayed);

    CloudRepublish.publish(relayed);

}

void imuCallback(const sensor_msgs::Imu::ConstPtr& imu_msg){

    sensor_msgs::Imu relayed;
    relayed = *imu_msg;
    //ImuRepublish.publish(relayed);
}


        /*       
        //------------------sensor topics from ouster -------------------------------
        /os1_node/imu_packets
        /os1_node/lidar_packets

        /os_cloud_node/imu
        /os_cloud_node/points

        /os_node/imu_packets
        /os_node/lidar_packets


        //---------------------topics expected by lios sam, modules(internal)

        nh.param<std::string>("/robot_id", robot_id, "roboat");

        nh.param<std::string>("lio_sam/pointCloudTopic", pointCloudTopic, "points_raw");
        nh.param<std::string>("lio_sam/imuTopic", imuTopic, "imu_correct");
        nh.param<std::string>("lio_sam/odomTopic", odomTopic, "odometry/imu");
        nh.param<std::string>("lio_sam/gpsTopic", gpsTopic, "odometry/gps");

        nh.param<std::string>("lio_sam/lidarFrame", lidarFrame, "base_link");
        nh.param<std::string>("lio_sam/baselinkFrame", baselinkFrame, "base_link");
        nh.param<std::string>("lio_sam/odometryFrame", odometryFrame, "odom");
        nh.param<std::string>("lio_sam/mapFrame", mapFrame, "map");

        nh.param<bool>("lio_sam/useImuHeadingInitialization", useImuHeadingInitialization, false);
        nh.param<bool>("lio_sam/useGpsElevation", useGpsElevation, false);
        nh.param<float>("lio_sam/gpsCovThreshold", gpsCovThreshold, 2.0);
        nh.param<float>("lio_sam/poseCovThreshold", poseCovThreshold, 25.0);

        */

#include <sstream>

int main(int argc, char **argv){

    ros::init(argc,argv, "challenge");
    ros::NodeHandle n;

    ros::Subscriber CloudSub = n.subscribe("/os_cloud_node/points",1, cloudCallback);
    ros::Subscriber ImuSub = n.subscribe("/os_cloud_node/imu",1, imuCallback);

    CloudRepublish = n.advertise<sensor_msgs::PointCloud2>("/points_raw",1);
    //ImuRepublish   = n.advertise<sensor_msgs::Imu>("/imu_raw",1);



    ros::Rate loop_rate(10);

    while(ros::ok()){

        ros::spinOnce();
        loop_rate.sleep();
        
    }

    return 0;
}
