#include <sstream>

#include "visualizer/VisualizerConstants.h"



main(int argc, char **argv)
{
    ros::init (argc, argv, "pcl_read");

    ROS_INFO("Started PCL read node");

    ros::NodeHandle nh;

    ros::Publisher GlobalMapPublisher = nh.advertise<sensor_msgs::PointCloud2> ("GlobalMap", 1);
    ros::Publisher SurfMapPublisher = nh.advertise<sensor_msgs::PointCloud2> ("SurfMap", 1);
    ros::Publisher CornerMapPublisher = nh.advertise<sensor_msgs::PointCloud2> ("CornerMap", 1);

    ros::Publisher trajectoryPublisher =  nh.advertise<nav_msgs::Path>("trajectory",1);
    ros::Publisher transformationsPublisher =  nh.advertise<nav_msgs::Path>("transformations",1);


   //-----------------------------GlobalMap /( or map frame )-------------------------

    sensor_msgs::PointCloud2 GlobalMapOutput;
    //pcl::PointCloud<pcl::PointXYZ> GlobalMapcloud;
    pcl::PointCloud<PointType> GlobalMapcloud;
    pcl::io::loadPCDFile (GlobalMap_PATH, GlobalMapcloud);

    pcl::toROSMsg(GlobalMapcloud, GlobalMapOutput);
    GlobalMapOutput.header.frame_id = "map";
   //-----------------------------SurfMap--------------------------------------------------
    
    sensor_msgs::PointCloud2 SurfMapOutput;
    pcl::PointCloud<PointType> SurfMapcloud;

    pcl::io::loadPCDFile (SurfMap_PATH, SurfMapcloud);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  

    pcl::toROSMsg(SurfMapcloud, SurfMapOutput);
    SurfMapOutput.header.frame_id = "SurfMap";

   //-----------------------------CornerMap-----------------------------------------
    
    sensor_msgs::PointCloud2 CornerMapOutput;
    pcl::PointCloud<PointType> CornerMapcloud;

    pcl::io::loadPCDFile (CornerMap_PATH, CornerMapcloud);

    pcl::toROSMsg(CornerMapcloud, CornerMapOutput);
    CornerMapOutput.header.frame_id = "CornerMap";


   //-----------------------------trajectory-----------------------------------------

    sensor_msgs::PointCloud2 trajectoryOutput;
    pcl::PointCloud<PointTypePose> trajectorycloud;

    pcl::io::loadPCDFile (trajectory_PATH, trajectorycloud);

    pcl::toROSMsg(trajectorycloud, trajectoryOutput);
    trajectoryOutput.header.frame_id = "trajectory";

   //-----------------------------transformations-----------------------------

    sensor_msgs::PointCloud2 transformationsOutput;
    pcl::PointCloud<PointTypePose> transformationscloud;

    pcl::io::loadPCDFile (transformations_PATH, transformationscloud);

    pcl::toROSMsg(transformationscloud, transformationsOutput);
    transformationsOutput.header.frame_id = "transformations";

    ///----------------------------------------------------------------------------



    ros::Rate loop_rate(1);
    while (ros::ok())
    {
        GlobalMapPublisher.publish(GlobalMapOutput);
        SurfMapPublisher.publish(SurfMapOutput);
        CornerMapPublisher.publish(CornerMapOutput);
        trajectoryPublisher.publish(trajectoryOutput);
        transformationsPublisher.publish(transformationsOutput);


        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
