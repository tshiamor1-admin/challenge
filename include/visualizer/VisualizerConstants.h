
#include "ros/ros.h"
#include "std_msgs/String.h"

//sensors messages
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Imu.h>

#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>

//pcl related
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>


#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>


#include <pcl/io/pcd_io.h>
#include <boost/foreach.hpp>

#define GlobalMap_PATH  "/home/ugv/catkin_ws/src/challenge/map/LOAM/GlobalMap.pcd"
#define SurfMap_PATH    "/home/ugv/catkin_ws/src/challenge/map/LOAM/SurfMap.pcd"
#define CornerMap_PATH  "/home/ugv/catkin_ws/src/challenge/map/LOAM/CornerMap.pcd"
#define trajectory_PATH "/home/ugv/catkin_ws/src/challenge/map/LOAM/trajectory.pcd"
#define transformations_PATH  "/home/ugv/catkin_ws/src/challenge/map/LOAM/transformations.pcd"


// #define GlobalMap_PATH  "/home/ugv/catkin_ws/src/challenge/map/LOAM_walking/GlobalMap.pcd"
// #define SurfMap_PATH    "/home/ugv/catkin_ws/src/challenge/map/LOAM_walking/SurfMap.pcd"
// #define CornerMap_PATH  "/home/ugv/catkin_ws/src/challenge/map/LOAM_walking/CornerMap.pcd"
// #define trajectory_PATH "/home/ugv/catkin_ws/src/challenge/map/LOAM_walking/trajectory.pcd"
// #define transformations_PATH  "/home/ugv/catkin_ws/src/challenge/map/LOAM_walking/transformations.pcd"


typedef pcl::PointXYZI PointType;

struct PointXYZIRPYT
{
    PCL_ADD_POINT4D
    PCL_ADD_INTENSITY;                  // preferred way of adding a XYZ+padding
    float roll;
    float pitch;
    float yaw;
    double time;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRPYT,
                                   (float, x, x) (float, y, y)
                                   (float, z, z) (float, intensity, intensity)
                                   (float, roll, roll) (float, pitch, pitch) (float, yaw, yaw)
                                   (double, time, time))

typedef PointXYZIRPYT  PointTypePose;